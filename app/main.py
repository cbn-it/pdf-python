import io
import os

from flask import Flask, request, abort, Response, make_response, send_from_directory
from subprocess import check_output, CalledProcessError
from random import randint
from googleapiclient import http
from oauth2client.service_account import ServiceAccountCredentials
from googleapiclient.discovery import build
import logging
import time
from re import match
from codecs import open

app = Flask(__name__, static_url_path='')
credentials = ServiceAccountCredentials.from_json_keyfile_name(
        '/app/AA Edu2bits-3372e9bf4800.json', ["https://www.googleapis.com/auth/devstorage.read_write"])
service = build('storage', 'v1', credentials=credentials)
log = logging.getLogger(__name__)
logging.basicConfig(format="[%(asctime)s]  %(levelname)s:%(message)s")
log.setLevel(logging.DEBUG)


# noinspection PyPep8Naming
@app.route("/render.pdf", methods=['GET', 'POST'])
def render_pdf():
    orientation = get_orientation()
    pageSize = getPageSize()
    url = request.values.get("url")
    fileName = getFileName()
    fileNameWithoutExtension = fileName[0:fileName.rfind(".")].replace(".", "_")
    zoom = getZoom()
    screenSize = getScreenSize()
    margin = getMargin()
    outputType = request.values.get("outputType")
    ts = str(int(time.time()*1000))
    if url is not None and not url == "":
        if not url.startswith("http://") and not url.startswith("https://"):
            url = "http://" + url

        command = "xvfb-run " \
                  "-s=\"{1}\" " \
                  "-a wkhtmltopdf " \
                  "--quiet " \
                  "--encoding UTF-8" \
                  "--disable-local-file-access " \
                  "--zoom {2} " \
                  "--orientation {3} " \
                  "--page-size {4} {5} {0} -".format(url, screenSize, zoom, orientation, pageSize, margin)
        log.info('Exec command %s' % command)
        pdf = execCommand(command)
    else:
        html = request.values.get("html")
        headerHtml = request.values.get("header")
        footerHtml = request.values.get("footer")
        if html is None:
            abort(404)

        random = str(randint(0, 30000))
        htmlUrl = createFileAndGetUrl(html, "html" + ts+"-"+random + ".html")
        headerUrl = createFileAndGetUrl(headerHtml, "header" +ts+"-"+random + ".html")
        footerUrl = createFileAndGetUrl(footerHtml, "footer" + ts+"-"+random + ".html")
        header = getHeader(headerUrl)
        footer = getFooter(footerUrl)
        command = "xvfb-run " \
                  "-s=\"{1}\" " \
                  "-a wkhtmltopdf " \
                  "--quiet --dpi 300 --disable-smart-shrinking " \
                  "--disable-local-file-access " \
                  "--zoom {2} --orientation {3} " \
                  "--page-size {4} {5} {6} {7} {0} -".format(htmlUrl, screenSize, zoom, orientation, pageSize, margin,
                                                             header, footer)
        log.info('Exec command %s' % command)
        pdf = execCommand(command)
        log.info('finished exec command')
    todayStr = time.strftime('%Y/%m/%d')
    filename = "pdf/" + todayStr + "/" + fileNameWithoutExtension + "-" + ts + ".pdf"
    media = http.MediaIoBaseUpload(io.BytesIO(pdf), 'application/pdf')
    req = service.objects().insert(
        name=filename,
        bucket="edu2bits.appspot.com",
        media_body=media,
        body={
            "acl": [{"entity": "allUsers", "role": "READER"}],
            "contentType": "application/pdf",
            "contentEncoding": "UTF8"
        })
    req.execute()
    if outputType is not None and outputType == "link":
        return Response("https://storage.googleapis.com/edu2bits.appspot.com/" + filename, mimetype='text/plain')
    else:
        response = make_response(pdf)
        response.headers[u"Content-Disposition"] = "attachment; filename=\"{0}\"".format(fileName)
        return response

# noinspection PyPep8Naming
@app.route("/")
def index():
    return app.send_static_file('index.html')


# noinspection PyPep8Naming
@app.route("/_ah/health")
def health_check():
    version = execCommand("wkhtmltopdf --version")
    if "0.12" in version:
        return 'ok'
    else:
        abort(400)


# noinspection PyPep8Naming
def execCommand(command):
    try:
        return check_output(command, shell=True)
    except CalledProcessError as e:
        return e.output


# noinspection PyPep8Naming
def getFooter(footerUrl):
    if footerUrl is None:
        return ""
    else:
        return " --footer-html " + footerUrl + " "


# noinspection PyPep8Naming
def getHeader(headerUrl):
    if headerUrl is None:
        return ""
    else:
        return " --header-html " + headerUrl + " "


# noinspection PyPep8Naming
def writeFile(fileName, html):
    f = open(fileName, 'w', "utf-8-sig")
    f.write(html)
    f.close()
    return f

# noinspection PyPep8Naming
def getNakedUrl():
    return request.base_url[:request.base_url.index("/", 8)]


# noinspection PyPep8Naming
def createFileAndGetUrl(html, filename):
    if html is None or html == "":
        return None
    html = html.strip()
    if not html.startswith("<!DOCTYPE html> "):
        html = "<!DOCTYPE html> \r\n" + html
    
    writeFile("/app/static/"+filename, html)
    return "file:///app/static/"+filename


# noinspection PyPep8Naming
def getFileName():
    file_name = request.values.get("filename")
    if (file_name is None) or (file_name == ""):
        file_name = "generated"
    elif file_name.endswith('.pdf'):
        return file_name
    extensie = request.values.get("format")
    if (extensie is None) or (extensie == ""):
        extensie = "pdf"
    
    return file_name + "." + extensie


# noinspection PyPep8Naming
def get_orientation():
    orientation = request.values.get('orientation')
    if (orientation is not None) and ("landscape" == orientation.lower()):
        return "Landscape"
    else:
        return "Portrait"


# noinspection PyPep8Naming
def getPageSize():
    page_size = "A4"
    pageSizeParam = request.values.get("pageSize")
    if (pageSizeParam is not None) and (pageSizeParam != "") and (match("[a-zA-Z0-9]{2,9}", pageSizeParam)):
        page_size = pageSizeParam
    return page_size


# noinspection PyPep8Naming
def getMargin():
    margin = " --margin-left " + getMarginParam("marginLeft") + "mm " \
             + "--margin-top " + getMarginParam("marginTop") + "mm " \
             + "--margin-right " + getMarginParam("marginRight") + "mm " \
             + "--margin-bottom " + getMarginParam("marginBottom") + "mm "
    return margin


# noinspection PyPep8Naming
def getMarginParam(side):
    try:
        margin = int(request.values.get(side), 10)
    except (ValueError, TypeError):
        margin = 10
    
    return str(margin)


# noinspection PyPep8Naming
def getScreenSize():
    try:
        screen_size_w = int(request.values.get("screenSizeW"), 10)
    except (ValueError, TypeError):
        screen_size_w = 1024
    
    try:
        screen_size_h = int(request.values.get("screenSizeH"), 10)
    except (ValueError, TypeError):
        screen_size_h = 768
    
    try:
        screen_size_c = int(request.values.get("screenSizeC"), 10)
    except (ValueError, TypeError):
        screen_size_c = 32
    
    return str(screen_size_w) + "x" + str(screen_size_h) + "x" + str(screen_size_c)


# noinspection PyPep8Naming
def getZoom():
    try:
        return str(float(request.values.get("zoom")))
    except (ValueError, TypeError):
        return "1.0"


PORT = int(os.environ.get('PORT',80))
app.run(host='0.0.0.0', port=PORT, debug=False, threaded=True, processes=1)

