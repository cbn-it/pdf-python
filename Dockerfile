FROM debian:jessie
MAINTAINER Bogdan Nourescu <bogdan.nourescu@cbn-it.ro>

RUN mkdir /images
ENV DEBIAN_FRONTEND=noninteractive

COPY [ "fonts", "/usr/local/share/fonts" ]
COPY [ "flash", "/usr/lib/mozilla/plugins/" ]
COPY [ "100-wkhtmltoimage-special.conf", "/etc/fonts/conf.d/100-wkhtmltoimage-special.conf" ]
ADD "https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.jessie_amd64.deb" \
    "/wkhtmltopdf/wkhtmltox_0.12.5-1.jessie_amd64.deb"


RUN apt-get update && \
    apt-get install -y wget openssl build-essential xorg libssl-dev xvfb libicu52 unzip python-dev python-pip && \
    pip install flask && \
    pip install --upgrade google-api-python-client oauth2client && \
    rm -rf /download/ && \
    dpkg -i /wkhtmltopdf/wkhtmltox_0.12.5-1.jessie_amd64.deb || apt-get -y -f install && \
    fc-cache -f

COPY [ "app", "/app" ]
EXPOSE 80

CMD [ "python", "/app/main.py" ]
