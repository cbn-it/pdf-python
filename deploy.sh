#!/bin/bash

APP_NAME=pdf-server
KUBE_RC_TEMPLATE=kube-rc.yml.template
KUBE_RC_RES=kube-rc.yml

CUR_RC=$(kubectl get rc -l app=$APP_NAME -o name)
CUR_RC=${CUR_RC##*/}

NEW_VERSION=1
if [ -n "$CUR_RC" ]; then
    NEW_VERSION=$(echo "$CUR_RC" | grep -oP "\-v\K[0-9]+$")
    NEW_VERSION=$(expr $NEW_VERSION + 1)
fi
echo "Upgrading resource controller $CUR_RC to v$NEW_VERSION"
cat "$KUBE_RC_TEMPLATE" | sed -e 's/{{RC_VERSION}}/v'$NEW_VERSION'/g' > "$KUBE_RC_RES"

if [ -z "$CUR_RC" ]; then
    echo "Creating resource controller..."
    kubectl create -f "$KUBE_RC_RES"
else
    kubectl rolling-update $CUR_RC -f "$KUBE_RC_RES"
fi
