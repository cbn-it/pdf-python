#!/bin/bash

SERVER_IMAGE="cbnit/pdf-server"
GCLOUD_TAG="eu.gcr.io/mso-converter/pdf-server"

if [ -n "$IMAGE_VERSION" ]; then
	GCLOUD_TAG="$GCLOUD_TAG:$IMAGE_VERSION"
fi
docker build -t "$SERVER_IMAGE" -t "$GCLOUD_TAG" .

if [ "$1" == "push" ]; then
	gcloud docker -- push "$GCLOUD_TAG"
fi
